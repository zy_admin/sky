package com.zy;

import com.zy.common.annotation.Excel;
import com.zy.system.entity.*;
import com.zy.system.mapper.*;
import com.zy.system.service.ISysUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SkyAdminApplication.class})
public class SkyAdminApplicationTests {

    @Autowired
    private SysUserMapper userMapper;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private SysDeptMapper deptMapper;

    @Autowired
    private SysRoleMapper roleMapper;

    @Autowired
    private SysMenuMapper menuMapper;

    @Autowired
    private SysDictDataMapper dataMapper;

    @Autowired
    private SysDictTypeMapper dictTypeMapper;

    @Autowired
    private SysNoticeMapper noticeMapper;

    @Autowired
    private SysConfigMapper configMapper;



    @Test
    public void contextLoads() {
        SysUser sysUser = userMapper.selectUserById(1L);
        System.out.println(sysUser);
    }

    @Test
    public void test2() {
        List<SysUser> list = userService.list();
        System.out.println(list);
    }

    @Test
    public void test3() {
        SysRole sysRole = roleMapper.selectRoleById(1L);
        SysRole sysRole1 = roleMapper.selectById(1L);
        System.out.println(sysRole);
    }

    @Test
    public void test4() {
        SysDept sysDept1 = deptMapper.selectDeptById(100L);
        SysDept sysDept = deptMapper.selectById(100L);
        System.out.println(sysDept1);
    }

    @Test
    public void test5() {
        SysMenu sysMenu = menuMapper.selectMenuById(1L);
        List<SysMenu> sysMenus = menuMapper.selectMenuAll();
        SysMenu sysMenu1 = menuMapper.selectById(1L);
        System.out.println(sysMenu1);
    }

    @Test
    public void test6() {
        SysDictData data = dataMapper.selectDictDataById(1L);
        SysDictData data1 = dataMapper.selectById(1L);
        System.out.println(data);
    }

    @Test
    public void test7() {
        SysDictType sysDictType = dictTypeMapper.selectDictTypeById(1L);
        SysDictType sysDictType1 = dictTypeMapper.selectById(1L);
        System.out.println(sysDictType);
    }

    @Test
    public void test8() {
        SysNotice sysNotice = noticeMapper.selectNoticeById(1L);
        SysNotice sysNotice1 = noticeMapper.selectById(1L);
        System.out.println(sysNotice);
    }

    @Test
    public void test9() {
        SysConfig sysConfig = configMapper.selectById(1L);
        System.out.println(sysConfig);
    }

}

