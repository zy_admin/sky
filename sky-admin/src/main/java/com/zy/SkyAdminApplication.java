package com.zy;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@MapperScan("com.zy.*.mapper")
//@EnableTransactionManagement
public class SkyAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(SkyAdminApplication.class, args);
        System.out.println("_______AAAA____s_k_y______AAAA________ \n" +
                "       VVVV               VVVV         \n" +
                "       (__)               (__)         \n" +
                "        \\ \\               / /          \n" +
                "         \\ \\   \\\\|||//   / /           \n" +
                "          > \\   _   _   / <            \n" +
                "           > \\ / \\ / \\ / <             \n" +
                "            > \\\\_o_o_// <              \n" +
                "             > ( (_) ) <               \n" +
                "              >|     |<                \n" +
                "             / |\\___/| \\               \n" +
                "             / (_____) \\               \n" +
                "             /         \\               \n" +
                "              /   o   \\                \n" +
                "               ) ___ (                 \n" +
                "              / /   \\ \\                \n" +
                "             ( /     \\ )               \n" +
                "             ><       ><               \n" +
                "            ///\\     /\\\\\\              \n" +
                "            '''       '''              \n" +
                "    project started successfully   ");
    }

}

