package com.zy.common.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.stereotype.Service;

/**
 * Package: com.zy.common.base.service
 * <p>
 * Author: zy
 * <p>
 * Date: Created in 2018-12-26 15:06
 */
public interface IBaseService<T> extends IService<T> {
}
