package com.zy.common.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
/**
 * Package: com.zy.common.base
 * <p>
 * Author: zy
 * <p>
 * Date: Created in 2018-12-31 15:02
 */
public interface IBaseMapper<T> extends BaseMapper<T> {
}
