package com.zy.common.base.service.impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zy.common.base.service.IBaseService;

/**
 * Package: com.zy.common.base.service.impl
 * <p>
 * Author: zy
 * <p>
 * Date: Created in 2018-12-26 15:07
 */
public class BaseServiceImpl<M extends BaseMapper<T>, T> extends ServiceImpl<M, T> implements IBaseService<T>{
}
