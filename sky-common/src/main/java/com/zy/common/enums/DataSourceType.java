package com.zy.common.enums;

/**
 * 数据源
 * 
 * @author zy
 */
public enum DataSourceType{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
