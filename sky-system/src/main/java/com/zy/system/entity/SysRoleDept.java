package com.zy.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zy.system.entity.base.BaseSysRoleDept;
import lombok.Data;

/**
 * 部门表
 * Package: com.zy.system.entity
 * <p>
 * Author: zy
 * <p>
 * Date: Created in 2018-12-24 21:43
 */
@Data
@TableName("sys_role_dept")
public class SysRoleDept extends BaseSysRoleDept {


}
