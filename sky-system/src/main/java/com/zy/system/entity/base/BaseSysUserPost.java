package com.zy.system.entity.base;

import com.baomidou.mybatisplus.annotation.TableId;
import com.zy.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * Package: com.zy.system.entity.base
 * <p>
 * Author: zy
 * <p>
 * Date: Created in 2018-12-26 21:07
 */
@Data
public class BaseSysUserPost extends BaseEntity {
    /**
     * 用户ID
     */
    @TableId
    private Long userId;

    /**
     * 岗位ID
     */
    @TableId
    private Long postId;
}
