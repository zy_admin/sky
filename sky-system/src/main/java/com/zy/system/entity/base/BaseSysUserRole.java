package com.zy.system.entity.base;

import com.baomidou.mybatisplus.annotation.TableId;
import com.zy.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * Package: com.zy.system.entity.base
 * <p>
 * Author: zy
 * <p>
 * Date: Created in 2018-12-26 21:06
 */
@Data
public class BaseSysUserRole extends BaseEntity {
    /** 用户ID */
    @TableId
    private Long userId;

    /** 角色ID */
    @TableId
    private Long roleId;
}
