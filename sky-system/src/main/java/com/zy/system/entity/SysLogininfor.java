package com.zy.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zy.system.entity.base.BaseSysLogininfor;
import lombok.Data;

import java.util.Date;

/**
 * 系统访问记录表 sys_logininfor
 *
 * @author zy
 */
@Data
@TableName("sys_logininfor")
public class SysLogininfor extends BaseSysLogininfor {

}