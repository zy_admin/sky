package com.zy.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zy.system.entity.base.BaseSysRoleMenu;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 角色-菜单
 * Package: com.zy.system.entity
 * <p>
 * Author: zy
 * <p>
 * Date: Created in 2018-12-26 14:34
 */
@Data
@TableName("sus_role_menu")
public class SysRoleMenu extends BaseSysRoleMenu {

}
