package com.zy.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zy.system.entity.base.BaseSysUserOnline;
import lombok.Data;

import java.util.Date;

/**
 * 当前在线会话 sys_user_online
 *
 * @author zy
 */
@Data
@TableName("sys_user_online")
public class SysUserOnline extends BaseSysUserOnline {

}
