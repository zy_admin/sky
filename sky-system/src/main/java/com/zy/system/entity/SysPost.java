package com.zy.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zy.system.entity.base.BaseSysPost;
import lombok.Data;

/**
 * 岗位表 sys_post
 *
 * @author zy
 */
@Data
@TableName("sys_post")
public class SysPost extends BaseSysPost {


}
