package com.zy.system.entity;

import com.zy.system.entity.base.BaseSysDictData;
import lombok.Data;

/**
 * 字典数据表 sys_dict_data
 *
 * @author zy
 */
@Data
public class SysDictData extends BaseSysDictData {

}
