package com.zy.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zy.system.entity.base.BaseSysMenu;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 菜单
 * Package: com.zy.system.entity
 * <p>
 * Author: zy
 * <p>
 * Date: Created in 2018-12-26 14:34
 */
@Data
@TableName("sys_menu")
public class SysMenu extends BaseSysMenu {

    /** 子菜单 */
    @TableField(exist = false)
    private List<SysMenu> children = new ArrayList<SysMenu>();

    /** 父菜单名称 */
    @TableField(exist = false)
    private String parentName;

}
