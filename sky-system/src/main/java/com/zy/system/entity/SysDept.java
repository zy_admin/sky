package com.zy.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zy.system.entity.base.BaseSysDept;
import lombok.Data;

/**
 * Package: com.zy.system.entity
 * <p>
 * Author: zy
 * <p>
 * Date: Created in 2018-12-24 21:04
 */
@Data
@TableName("sys_dept")
public class SysDept extends BaseSysDept {

    /** 父部门名称 */
    @TableField(exist = false)
    private String parentName;

}
