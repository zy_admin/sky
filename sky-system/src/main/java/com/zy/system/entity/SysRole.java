package com.zy.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zy.system.entity.base.BaseSysRole;
import lombok.Data;

/**
 * 角色表
 * Package: com.zy.system.entity
 * <p>
 * Author: zy
 * <p>
 * Date: Created in 2018-12-24 21:05
 */
@Data
@TableName("sys_role")
public class SysRole extends BaseSysRole {

    /** 菜单组 */
    @TableField(exist = false)
    private Long[] menuIds;

    /** 部门组（数据权限） */
    @TableField(exist = false)
    private Long[] deptIds;

    /** 用户是否存在此角色标识 默认不存在 */
    @TableField(exist = false)
    private boolean flag = false;
}
