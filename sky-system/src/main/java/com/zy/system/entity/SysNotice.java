package com.zy.system.entity;

import com.zy.system.entity.base.BaseSysNotice;
import lombok.Data;

/**
 * 通知公告表 sys_notice
 *
 * @author zy
 */
@Data
public class SysNotice extends BaseSysNotice {

}
