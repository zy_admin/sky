package com.zy.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zy.system.entity.base.BaseSysUserRole;
import lombok.Data;

/**用户-角色关联表
 * Package: com.zy.system.entity
 * <p>
 * Author: zy
 * <p>
 * Date: Created in 2018-12-24 21:43
 */
@Data
@TableName("sys_user_role")
public class SysUserRole extends BaseSysUserRole {

}
