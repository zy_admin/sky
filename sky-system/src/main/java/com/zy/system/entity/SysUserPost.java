package com.zy.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zy.system.entity.base.BaseSysUserPost;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 用户和岗位关联 sys_user_post
 *
 * @author zy
 */
@Data
@TableName("sys_user_post")
public class SysUserPost extends BaseSysUserPost {

}
