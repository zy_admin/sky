package com.zy.system.entity.base;

import com.baomidou.mybatisplus.annotation.TableId;
import com.zy.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * Package: com.zy.system.entity.base
 * <p>
 * Author: zy
 * <p>
 * Date: Created in 2018-12-26 21:18
 */
@Data
public class BaseSysRoleMenu extends BaseEntity {
    /** 角色ID */
    @TableId
    private Long roleId;

    /** 菜单ID */
    @TableId
    private Long menuId;
}
