package com.zy.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zy.common.annotation.Excel;
import com.zy.common.annotation.Excel.Type;
import com.zy.system.entity.base.BaseSysUser;
import lombok.Data;

import java.util.List;

/**
 * 用户表
 * Package: com.zy.system.entity
 * <p>
 * Author: zy
 * <p>
 * Date: Created in 2018-12-24 20:59
 */
@Data
@TableName("sys_user")
public class SysUser extends BaseSysUser {

    /**
     * 部门ID
     */
    @TableField(exist = false)
    @Excel(name = "部门编号", type = Type.IMPORT)
    private Long deptId;

    /**
     * 部门父ID
     */
    @TableField(exist = false)
    private Long parentId;

    /**
     * 部门对象
     */
    @Excel(name = "部门名称", targetAttr = "deptName", type = Type.EXPORT)
    @TableField(exist = false)
    private SysDept dept;

    @TableField(exist = false)
    private List<SysRole> roles;

    /**
     * 角色组
     */
    @TableField(exist = false)
    private Long[] roleIds;

    /**
     * 岗位组
     */
    @TableField(exist = false)
    private Long[] postIds;

    /**
     * 防止导出部门的时候对象为null
     * @return
     */
    public SysDept getDept() {
        if (dept == null) {
            dept = new SysDept();
        }
        return dept;
    }
}
