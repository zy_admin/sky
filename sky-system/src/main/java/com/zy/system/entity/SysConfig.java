package com.zy.system.entity;


import com.zy.system.entity.base.BaseSysConfig;
import lombok.Data;
/**
 * 参数配置表 sys_config
 *
 * @author zy
 */
@Data
public class SysConfig extends BaseSysConfig {

}
