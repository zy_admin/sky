package com.zy.system.entity;

import com.zy.system.entity.base.BaseSysDictType;
import lombok.Data;

/**
 * 字典类型表 sys_dict_type
 *
 * @author zy
 */
@Data
public class SysDictType extends BaseSysDictType {

}
