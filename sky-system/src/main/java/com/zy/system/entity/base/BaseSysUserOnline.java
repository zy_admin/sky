package com.zy.system.entity.base;

import com.baomidou.mybatisplus.annotation.TableId;
import com.zy.common.core.domain.BaseEntity;
import com.zy.common.enums.OnlineStatus;
import lombok.Data;

import java.util.Date;

/**
 * Package: com.zy.system.entity.base
 * <p>
 * Author: zy
 * <p>
 * Date: Created in 2018-12-26 21:10
 */
@Data
public class BaseSysUserOnline extends BaseEntity {
    /**
     * 用户会话id
     */
    @TableId
    private String sessionId;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 登录名称
     */
    private String loginName;

    /**
     * 登录IP地址
     */
    private String ipaddr;

    /**
     * 登录地址
     */
    private String loginLocation;

    /**
     * 浏览器类型
     */
    private String browser;

    /**
     * 操作系统
     */
    private String os;

    /**
     * session创建时间
     */
    private Date startTimestamp;

    /**
     * session最后访问时间
     */
    private Date lastAccessTime;

    /**
     * 超时时间，单位为分钟
     */
    private Long expireTime;

    /**
     * 在线状态
     */
    private OnlineStatus status = OnlineStatus.on_line;
}
