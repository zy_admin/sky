package com.zy.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zy.system.entity.base.BaseSysOperLog;
import lombok.Data;

/**
 * 操作日志记录表 oper_log
 *
 * @author zy
 */
@Data
@TableName("sys_oper_log")
public class SysOperLog extends BaseSysOperLog {

}
