package com.zy.system.entity.base;

import com.baomidou.mybatisplus.annotation.TableId;
import com.zy.common.annotation.Excel;
import com.zy.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * Package: com.zy.system.entity.base
 * <p>
 * Author: zy
 * <p>
 * Date: Created in 2018-12-31 14:58
 */
@Data
public class BaseSysConfig extends BaseEntity {

    /**
     * 参数主键
     */
    @Excel(name = "参数主键")
    @TableId
    private Long configId;

    /**
     * 参数名称
     */
    @Excel(name = "参数名称")
    private String configName;

    /**
     * 参数键名
     */
    @Excel(name = "参数键名")
    private String configKey;

    /**
     * 参数键值
     */
    @Excel(name = "参数键值")
    private String configValue;

    /**
     * 系统内置（Y是 N否）
     */
    @Excel(name = "系统内置", readConverterExp = "Y=是,N=否")
    private String configType;
}
