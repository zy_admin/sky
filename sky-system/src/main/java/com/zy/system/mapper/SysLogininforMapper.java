package com.zy.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zy.common.base.mapper.IBaseMapper;
import com.zy.system.entity.SysLogininfor;

import java.util.List;

/**
 * Package: com.zy.system.mapper
 * <p>
 * Author: zy
 * <p>
 * Date: Created in 2018-12-26 20:34
 */
public interface SysLogininforMapper extends IBaseMapper<SysLogininfor> {
    /**
     * 新增系统登录日志
     *
     * @param logininfor 访问日志对象
     */
    void insertLogininfor(SysLogininfor logininfor);

    /**
     * 查询系统登录日志集合
     *
     * @param logininfor 访问日志对象
     * @return 登录记录集合
     */
    List<SysLogininfor> selectLogininforList(SysLogininfor logininfor);

    /**
     * 批量删除系统登录日志
     *
     * @param ids 需要删除的数据
     * @return 结果
     */
    int deleteLogininforByIds(String[] ids);

    /**
     * 清空系统登录日志
     *
     * @return 结果
     */
    int cleanLogininfor();
}
