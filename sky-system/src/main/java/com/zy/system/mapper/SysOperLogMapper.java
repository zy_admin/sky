package com.zy.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zy.common.base.mapper.IBaseMapper;
import com.zy.system.entity.SysOperLog;

import java.util.List;

/**
 * Package: com.zy.system.mapper
 * <p>
 * Author: zy
 * <p>
 * Date: Created in 2018-12-26 20:34
 */
public interface SysOperLogMapper extends IBaseMapper<SysOperLog> {
    /**
     * 新增操作日志
     *
     * @param operLog 操作日志对象
     */
    void insertOperlog(SysOperLog operLog);

    /**
     * 查询系统操作日志集合
     *
     * @param operLog 操作日志对象
     * @return 操作日志集合
     */
    List<SysOperLog> selectOperLogList(SysOperLog operLog);

    /**
     * 批量删除系统操作日志
     *
     * @param ids 需要删除的数据
     * @return 结果
     */
    int deleteOperLogByIds(String[] ids);

    /**
     * 查询操作日志详细
     *
     * @param operId 操作ID
     * @return 操作日志对象
     */
    SysOperLog selectOperLogById(Long operId);

    /**
     * 清空操作日志
     */
    void cleanOperLog();
}
