package com.zy.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zy.common.base.mapper.IBaseMapper;
import com.zy.system.entity.SysRoleDept;

import java.util.List;

/**
 * Package: com.zy.system.mapper
 * <p>
 * Author: zy
 * <p>
 * Date: Created in 2018-12-26 14:52
 */
public interface SysRoleDeptMapper extends IBaseMapper<SysRoleDept> {
    /**
     * 通过角色ID删除角色和部门关联
     *
     * @param roleId 角色ID
     * @return 结果
     */
    int deleteRoleDeptByRoleId(Long roleId);

    /**
     * 批量删除角色部门关联信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deleteRoleDept(Long[] ids);

    /**
     * 查询部门使用数量
     *
     * @param deptId 部门ID
     * @return 结果
     */
    int selectCountRoleDeptByDeptId(Long deptId);

    /**
     * 批量新增角色部门信息
     *
     * @param roleDeptList 角色部门列表
     * @return 结果
     */
    int batchRoleDept(List<SysRoleDept> roleDeptList);
}
