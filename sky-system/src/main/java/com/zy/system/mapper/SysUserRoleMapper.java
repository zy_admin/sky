package com.zy.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zy.common.base.mapper.IBaseMapper;
import com.zy.system.entity.SysRoleMenu;
import com.zy.system.entity.SysUserRole;

import java.util.List;

/**
 * Package: com.zy.system.mapper
 * <p>
 * Author: zy
 * <p>
 * Date: Created in 2018-12-26 14:52
 */
public interface SysUserRoleMapper extends IBaseMapper<SysUserRole> {

    /**
     * 通过用户ID删除用户和角色关联
     *
     * @param userId 用户ID
     * @return 结果
     */
    int deleteUserRoleByUserId(Long userId);

    /**
     * 批量删除用户和角色关联
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deleteUserRole(Long[] ids);

    /**
     * 通过角色ID查询角色使用数量
     *
     * @param roleId 角色ID
     * @return 结果
     */
    int countUserRoleByRoleId(Long roleId);

    /**
     * 批量新增用户角色信息
     *
     * @param userRoleList 用户角色列表
     * @return 结果
     */
    int batchUserRole(List<SysUserRole> userRoleList);
}
