package com.zy.system;

import com.zy.system.entity.SysUser;
import com.zy.system.mapper.SysUserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SkySystemApplicationTests {
    @Autowired
    private SysUserMapper userMapper;

    @Test
    public void contextLoads() {
        SysUser sysUser = userMapper.selectUserById(1L);
        System.out.println(sysUser);
    }

}

